package com.example.tucasitacalidade.network

import androidx.room.*
import com.example.tucasitacalidade.modelo.Usuario

@Dao
interface UsuarioDao {

    @Insert
    suspend fun insertar(usuario:Usuario)

    @Update
    suspend fun update(usuario: Usuario)

    @Delete
    suspend fun deleteUsuario(usuario:Usuario)

    @Query("SELECT * FROM usuarios")
    suspend fun getAll():List<Usuario>

    @Query( "SELECT * FROM usuarios WHERE id = :id")
    suspend fun getUsuario(id: Int): Usuario
}