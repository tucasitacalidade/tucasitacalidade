package com.example.tucasitacalidade.network

import android.content.Context
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleCoroutineScope
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.tucasitacalidade.modelo.Usuario
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.internal.synchronized

@Database( entities = [Usuario::class],version = 1)
abstract class UsuarioDB: RoomDatabase() {
    abstract fun usuarioDao(): UsuarioDao

    // nuestro singleton
    companion object{
        @Volatile
        private var INSTANCE: UsuarioDB? = null

        @InternalCoroutinesApi
        fun getDatabase(context: Context): UsuarioDB {
            val instanciaTempo = INSTANCE
            if(instanciaTempo != null){
                return instanciaTempo
            }

            synchronized(this){
                val instancia = Room.databaseBuilder(context.applicationContext,UsuarioDB::class.java,"usuario")
                    .build()

                INSTANCE = instancia
                return instancia
            }



        }
    }
}