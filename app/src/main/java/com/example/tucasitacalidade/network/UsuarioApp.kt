package com.example.tucasitacalidade.network

import android.app.Application
import androidx.room.Room

class UsuarioApp:Application() {
    val room = Room.databaseBuilder(applicationContext,UsuarioDB::class.java,"usuario").build()
}