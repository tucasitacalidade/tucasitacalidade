package com.example.tucasitacalidade.modelo

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "usuarios")
data class Usuario (
    @PrimaryKey(autoGenerate = true)
    val id:Int = 0,
    val nom:String,
    val edad:Int
    )
