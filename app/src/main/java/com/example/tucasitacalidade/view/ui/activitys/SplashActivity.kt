package com.example.tucasitacalidade.view.ui.activitys

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.tucasitacalidade.R

class SplashActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
    }
}