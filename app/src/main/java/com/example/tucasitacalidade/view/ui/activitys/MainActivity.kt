package com.example.tucasitacalidade.view.ui.activitys

import android.app.Application
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import android.widget.Toolbar
import androidx.lifecycle.lifecycleScope
import com.example.tucasitacalidade.R
import com.example.tucasitacalidade.modelo.Usuario
import com.example.tucasitacalidade.network.UsuarioApp
import com.example.tucasitacalidade.network.UsuarioDB
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.launch

class MainActivity : AppCompatActivity() {


    @InternalCoroutinesApi
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        //val app = applicationContext as UsuarioApp

        val usuario = Usuario(0,"javi",66)
        val database = UsuarioDB.getDatabase(this)
        lifecycleScope.launch {
            database.usuarioDao().insertar(usuario)

        }


        Toast.makeText(this,"el usuario esta añadido",Toast.LENGTH_SHORT).show()


    }
}